package com.wetic.jersey.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.wetic.jersey.domain.Depense;

@SuppressWarnings("unused")
@Repository
public interface DepenseRepository extends JpaRepository<Depense, Long> {
	
	@Override
	@Query(value="select d.id, d.date_facturation, d.description, d.libelle, d.montant, td.libelle from depense d join type_depense td  on d.type_depense_id = td.id",nativeQuery=true)
	public Page<Depense>findAll(Pageable pageable);
	
	@Query(value="select sum(montant) from depense",nativeQuery=true)
	public Float SumAll(Float montant);
	

}
