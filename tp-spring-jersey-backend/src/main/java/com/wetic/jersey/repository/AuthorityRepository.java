package com.wetic.jersey.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wetic.jersey.domain.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, String> {
	
	Optional<Authority> findByName(String name);

}
