package com.wetic.jersey.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.wetic.jersey.domain.Client;
import com.wetic.jersey.domain.Facture;
import com.wetic.jersey.service.dto.ClientDTO;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

	@Override
	@Query(value = "SELECT c.*, u.first_name FROM client c JOIN jhi_user u ON c.user_id = u.id", nativeQuery = true)
	public Page<Client> findAll(Pageable pageable);

	@Query(value = "select count(*) as countClient from client", nativeQuery = true)
	public Long CountAll();

	public Page<Client> findByUser(Pageable pageable, Long id);

}
