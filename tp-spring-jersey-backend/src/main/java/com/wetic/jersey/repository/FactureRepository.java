package com.wetic.jersey.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.wetic.jersey.domain.Facture;

@SuppressWarnings("unused")
@Repository
public interface FactureRepository extends JpaRepository<Facture, Long> {

//	@Override
	@Query(value = "select facture.id as id, date_facturation, facture.client_id, first_name from facture join client on facture.client_id = client.id join jhi_user on client.user_id = jhi_user.id", nativeQuery = true)
	public Page<Facture> findAll(Pageable pageable);

	 @Query(value="select * from facture where client_id= :id",nativeQuery=true)
	public Page<Facture> findByClient(Pageable pageable, @Param(value = "id") Long id);

	@Query(value = "select count(*) as countFacture from facture", nativeQuery = true)
	public Long CountAll();

}
