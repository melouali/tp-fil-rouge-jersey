package com.wetic.jersey.service.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class DetailsFactureDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotNull
	private int qteProduit;
	
	@NotNull
	private String description;
	
	@NotNull
	private String produitName;
	
	@NotNull
	private float produitPrix;
	
	@NotNull
	private Long produitId;
	
	@NotNull
	private Long factureId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getQteProduit() {
		return qteProduit;
	}

	public void setQteProduit(int qteProduit) {
		this.qteProduit = qteProduit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getProduitId() {
		return produitId;
	}

	public void setProduitId(Long produitId) {
		this.produitId = produitId;
	}

	public Long getFactureId() {
		return factureId;
	}

	public void setFactureId(Long factureId) {
		this.factureId = factureId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((factureId == null) ? 0 : factureId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((produitId == null) ? 0 : produitId.hashCode());
		result = prime * result + qteProduit;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetailsFactureDTO other = (DetailsFactureDTO) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (factureId == null) {
			if (other.factureId != null)
				return false;
		} else if (!factureId.equals(other.factureId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (produitId == null) {
			if (other.produitId != null)
				return false;
		} else if (!produitId.equals(other.produitId))
			return false;
		if (qteProduit != other.qteProduit)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DetailsFactureDTO {id=" + getId() + ", qteProduit=" + getQteProduit() + ", description=" + getDescription()
				+ ", produitId=" + getProduitId() + ", factureId=" + getFactureId() + "}";
	}
	
	
}
