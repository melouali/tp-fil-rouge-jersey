package com.wetic.jersey.service.dto;

import java.util.Objects;

public class ProduitDTO {

	private Long id;
	
	private String description;
	
	private String libelle;
	
	private float prix;
	
	private int qteStock;
	
	
	// Accesseurs et mutateurs
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	public int getQteStock() {
		return qteStock;
	}

	public void setQteStock(int qteStock) {
		this.qteStock = qteStock;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProduitDTO other = (ProduitDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}
	

	@Override
	public String toString() {
		return "ProduitDTO [id=" + id + ", description=" + description + ", libelle=" + libelle + ", prix=" + prix
				+ ", qteStock=" + qteStock + "]";
	}
	
	
}
