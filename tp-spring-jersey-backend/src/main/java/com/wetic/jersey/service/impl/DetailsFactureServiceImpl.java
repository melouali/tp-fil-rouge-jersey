package com.wetic.jersey.service.impl;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wetic.jersey.domain.DetailsFacture;
import com.wetic.jersey.repository.DetailsFactureRepository;
import com.wetic.jersey.service.DetailsFactureService;
import com.wetic.jersey.service.dto.DetailsFactureDTO;
import com.wetic.jersey.service.mapper.DetailsFactureMapper;


@Service
public class DetailsFactureServiceImpl implements DetailsFactureService {

	private final DetailsFactureRepository detailsFactureRepository;
	
	private final DetailsFactureMapper detailsFactureMapper;
	
	public DetailsFactureServiceImpl(DetailsFactureRepository detailsFactureRepository, DetailsFactureMapper detailsFactureMapper) {
		this.detailsFactureMapper = detailsFactureMapper;
		this.detailsFactureRepository = detailsFactureRepository;
	}
	
	@Override
	public DetailsFactureDTO save(DetailsFactureDTO detailsFactureDTO) {
		DetailsFacture detailsFacture = detailsFactureMapper.toEntity(detailsFactureDTO);
		detailsFacture = detailsFactureRepository.save(detailsFacture);
		return detailsFactureMapper.toDto(detailsFacture);
	}

	@Override
	public Page<DetailsFactureDTO> findAll(Pageable pageable) {
		return detailsFactureRepository.findAll(pageable).map(detailsFactureMapper::toDto);
	}

	@Override
	public Optional<DetailsFactureDTO> findOne(Long id) {
		return detailsFactureRepository.findById(id).map(detailsFactureMapper::toDto);
	}

	@Override
	public void delete(Long id) {
		detailsFactureRepository.deleteById(id);
	}

	@Override
	public Page<DetailsFactureDTO> findByFactureId(Pageable pageable, Long id){
		return detailsFactureRepository.findDetailsFactureByFactureId(pageable, id).map(detailsFactureMapper::toDto);
	}
}
