package com.wetic.jersey.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wetic.jersey.service.dto.ClientDTO;

public interface ClientService {

	ClientDTO save(ClientDTO clientDTO);

	Page<ClientDTO> findAll(Pageable pageable);

	Page<ClientDTO> findByUser(Pageable pageable, Long id);

	Optional<ClientDTO> findOne(Long id);

	void delete(Long id);

}
