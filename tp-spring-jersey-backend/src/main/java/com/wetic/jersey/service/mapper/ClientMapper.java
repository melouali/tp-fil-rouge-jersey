package com.wetic.jersey.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wetic.jersey.domain.Client;
import com.wetic.jersey.service.dto.ClientDTO;

/**
 * Mapper for the entity {@link Client} and its DTO {@link ClientDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })

public interface ClientMapper extends EntityMapper<ClientDTO, Client> {
    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.firstName", target = "clientName")
	ClientDTO toDto(Client client);

    @Mapping(source = "userId", target = "user")
	Client toEntity(ClientDTO clientDTO);

	default Client fromId(Long id) {
		if (id == null) {
			return null;
		}
		Client client = new Client();
		client.setId(id);
		return client;
	}
}
