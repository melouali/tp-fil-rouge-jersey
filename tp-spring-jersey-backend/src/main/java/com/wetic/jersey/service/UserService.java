package com.wetic.jersey.service;

import java.time.Instant;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wetic.jersey.config.Constants;
import com.wetic.jersey.domain.Authority;
import com.wetic.jersey.domain.User;
import com.wetic.jersey.repository.AuthorityRepository;
import com.wetic.jersey.repository.ClientRepository;
import com.wetic.jersey.repository.DepenseRepository;
import com.wetic.jersey.repository.FactureRepository;
import com.wetic.jersey.repository.ProduitRepository;
import com.wetic.jersey.repository.UserRepository;
import com.wetic.jersey.service.dto.UserDTO;
import com.wetic.jersey.service.util.RandomUtil;

/**
 * Service class for managing users.
 */
@Service
// @Transactional
public class UserService {

	private final PasswordEncoder passwordEncoder;

	private final UserRepository userRepository;

	private final DepenseRepository depenseRepository;

	private final ProduitRepository produitRepository;

	private final FactureRepository factureRepository;

	private final ClientRepository clientRepository;

	private final AuthorityRepository authorityRepository;

	// private final PasswordEncoder passwordEncoder;

	// private final CacheManager cacheManager;

    public UserService(UserRepository userRepository,
    		PasswordEncoder passwordEncoder,
    		DepenseRepository depenseRepository,
    		ProduitRepository produitRepository,
    		FactureRepository factureRepository,
            AuthorityRepository authorityRepository,
    		ClientRepository clientRepository
    		) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
//        this.cacheManager = cacheManager;
        this.depenseRepository = depenseRepository;
        this.produitRepository = produitRepository;
        this.factureRepository = factureRepository;
        this.authorityRepository = authorityRepository;
        this.clientRepository = clientRepository;
    }

	public Optional<User> activateRegistration(String key) {
		return userRepository.findOneByActivationKey(key).map(user -> {
			// activate given user for the registration key.
			user.setActivated(true);
			user.setActivationKey(null);
			return user;
		});
	}

	private boolean removeNonActivatedUser(User existingUser) {
		if (existingUser.getActivated()) {
			return false;
		}
		userRepository.delete(existingUser);
		userRepository.flush();
		return true;
	}

    public User createUser(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail().toLowerCase());
        user.setImageUrl(userDTO.getImageURL());
        if (userDTO.getLangKey() == null) {
            user.setLangKey("en"); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        final String encryptedPassword = passwordEncoder.encode(userDTO.getPassword());
        user.setPassword(encryptedPassword);
        final Set<Authority> managedAuthorities = user.getAuthorities();
        managedAuthorities.clear();
        userDTO.getAuthorities().stream()
            .map(authorityRepository::findById)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .forEach(managedAuthorities::add);
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
       
        final Set<Authority> managedAuthorities = user.getAuthorities();
        managedAuthorities.clear();
        userDTO.getAuthorities().stream()
        .map(authorityRepository::findById)
        .filter(Optional::isPresent)
        .map(Optional::get)
        .forEach(managedAuthorities::add);
		
		userRepository.save(user);
		return user;
	}

	/**
	 * Update all information for a specific user, and return the modified user.
	 *
	 * @param userDTO
	 *            user to update.
	 * @return updated user.
	 */
	public Optional<UserDTO> updateUser(UserDTO userDTO) {
		return Optional.of(userRepository.findById(userDTO.getId())).filter(Optional::isPresent).map(Optional::get)
				.map(user -> {
					user.setLogin(userDTO.getLogin().toLowerCase());
					user.setFirstName(userDTO.getFirstName());
					user.setLastName(userDTO.getLastName());
					user.setEmail(userDTO.getEmail().toLowerCase());
					user.setImageUrl(userDTO.getImageURL());
					user.setActivated(userDTO.isActivated());
					user.setLangKey(userDTO.getLangKey());

					final Set<Authority> managedAuthorities = user.getAuthorities();
					managedAuthorities.clear();
					userDTO.getAuthorities().stream().map(authorityRepository::findById).filter(Optional::isPresent)
							.map(Optional::get).forEach(managedAuthorities::add);


                return user;
            })
            .map(UserDTO::new);
    }

	public void deleteUser(String login) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
			userRepository.delete(user);
		});
	}

	public Optional<User> getUserById(long id) {
		return userRepository.findOneById(id);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthoritiesByLogin(String login) {
		return userRepository.findOneWithAuthoritiesByLogin(login);
	}

}
