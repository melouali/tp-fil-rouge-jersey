package com.wetic.jersey.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wetic.jersey.service.dto.ProduitDTO;

public interface ProduitService {

	/**
	 * Save a product
	 * 
	 * @param produitDTO for the datas to transmit to entity
	 * @return the persisted entity
	 */
	ProduitDTO save (ProduitDTO produitDTO);
	
	
	/**
	 * A list of all the products
	 * 
	 * @param pageable to display all results
	 * @return the list of products
	 */
	Page<ProduitDTO> findAll(Pageable pageable);
	
	
	/**
	 * Find one product
	 * 
	 * @param id to find the product by id
	 * @return the product concerned if exists
	 */
	Optional<ProduitDTO> findOne(Long id);
	
	/**
	 * delete a product from database
	 * 
	 * @param id to select the product concerned
	 */
	void delete(Long id);
	
	
}
