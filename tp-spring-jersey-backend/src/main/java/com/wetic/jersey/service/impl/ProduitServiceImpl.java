package com.wetic.jersey.service.impl;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wetic.jersey.domain.Produit;
import com.wetic.jersey.repository.ProduitRepository;
import com.wetic.jersey.service.ProduitService;
import com.wetic.jersey.service.dto.ProduitDTO;
import com.wetic.jersey.service.mapper.ProduitMapper;

@Service
public class ProduitServiceImpl implements ProduitService{
	
	private final ProduitRepository produitRepository;
	
	private final ProduitMapper produitMapper;
	
	public ProduitServiceImpl(ProduitRepository produitRepository, ProduitMapper produitMapper) {
		this.produitRepository = produitRepository;
		this.produitMapper = produitMapper;
	}

	@Override
	public ProduitDTO save(ProduitDTO produitDTO) {
		 Produit produit = produitMapper.toEntity(produitDTO);
	        produit = produitRepository.save(produit);
	        return produitMapper.toDto(produit);
	}


	@Override
	public Optional<ProduitDTO> findOne(Long id) {
	     return produitRepository.findById(id)
	             .map(produitMapper::toDto);
	}

	@Override
	public void delete(Long id) {
		produitRepository.deleteById(id);
	}

	@Override
	public Page<ProduitDTO> findAll(Pageable pageable) {
		return produitRepository.findAll(pageable)
	            .map(produitMapper::toDto);
	}



}
