package com.wetic.jersey.service.impl;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.wetic.jersey.domain.Depense;
import com.wetic.jersey.repository.DepenseRepository;
import com.wetic.jersey.service.DepensesService;
import com.wetic.jersey.service.dto.DepensesDTO;
import com.wetic.jersey.service.mapper.DepensesMapper;

/**
 * Service Implementation for managing {@link Depense}.
 */
@Service
public class DepensesServiceImpl implements DepensesService {

	private final DepenseRepository depenseRepository;
	private final DepensesMapper depensesMapper;

	public DepensesServiceImpl(DepenseRepository depenseRepository, DepensesMapper depensesMapper) {
		super();
		this.depenseRepository = depenseRepository;
		this.depensesMapper = depensesMapper;
	}

	@Override
	public DepensesDTO save(DepensesDTO depensesDTO) {
		Depense depense = depensesMapper.toEntity(depensesDTO);
		depense = depenseRepository.save(depense);
		return depensesMapper.toDto(depense);
	}

	@Override
	public Page<DepensesDTO> findAll(Pageable pageable) {
		return depenseRepository.findAll(pageable).map(depensesMapper::toDto);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<DepensesDTO> findOne(Long id) {
		return depenseRepository.findById(id).map(depensesMapper::toDto);
	}

	@Override
	public void delete(Long id) {

		depenseRepository.deleteById(id);
	}

}
