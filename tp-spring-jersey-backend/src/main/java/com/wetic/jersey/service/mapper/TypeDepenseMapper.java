package com.wetic.jersey.service.mapper;

import org.mapstruct.*;

import com.wetic.jersey.domain.TypeDepense;
import com.wetic.jersey.service.dto.TypeDepenseDTO;

@Mapper(componentModel = "spring", uses = {})
public interface TypeDepenseMapper extends EntityMapper<TypeDepenseDTO, TypeDepense> {

    
	TypeDepense toEntity(TypeDepenseDTO typeDepenseDTO);

    @Mapping(source = "typeDepenseId", target = "typeDepense")
	default TypeDepense fromId(Long id) {
		if (id == null) {
			return null;
		}
		TypeDepense typeDepense = new TypeDepense();
		typeDepense.setId(id);
		return typeDepense;
	}
}
