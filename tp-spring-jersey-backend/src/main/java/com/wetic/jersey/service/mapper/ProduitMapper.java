package com.wetic.jersey.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.wetic.jersey.domain.Produit;
import com.wetic.jersey.service.dto.ProduitDTO;

@Mapper(componentModel = "spring", uses = {})
public interface ProduitMapper extends EntityMapper<ProduitDTO, Produit> {

	@Mapping(target = "detailsFacture", ignore = true)
	Produit toEntity(ProduitDTO produitDTO);
	
	@Mapping(source = "produitId", target = "produit")
	default Produit fromId(Long id) {
        if (id == null) {
            return null;
        }
        Produit produit = new Produit();
        produit.setId(id);
        return produit;
    }

}
