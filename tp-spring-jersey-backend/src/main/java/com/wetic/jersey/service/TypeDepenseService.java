package com.wetic.jersey.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wetic.jersey.service.dto.TypeDepenseDTO;

public interface TypeDepenseService {

	TypeDepenseDTO save(TypeDepenseDTO typeDepenseDTO);

	Page<TypeDepenseDTO> findAll(Pageable pageable);

	Optional<TypeDepenseDTO> findOne(Long id);

	void delete(Long id);

}
