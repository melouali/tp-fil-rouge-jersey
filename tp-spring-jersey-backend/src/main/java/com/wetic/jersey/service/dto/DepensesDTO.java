package com.wetic.jersey.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.validation.constraints.NotNull;

public class DepensesDTO implements Serializable {

	private Long id;

    @NotNull
    private String description;
   
    @NotNull
    private String libelle;
    
    @NotNull
    private String montant;

    @NotNull
    private ZonedDateTime date;


    private Long typeDepenseId;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getLibelle() {
		return libelle;
	}


	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}


	public String getMontant() {
		return montant;
	}


	public void setMontant(String montant) {
		this.montant = montant;
	}


	public ZonedDateTime getDate() {
		return date;
	}


	public void setDate(ZonedDateTime date) {
		this.date = date;
	}


	public Long getTypeDepenseId() {
		return typeDepenseId;
	}


	public void setTypeDepenseId(Long typeDepenseId) {
		this.typeDepenseId = typeDepenseId;
	}
    
	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DepensesDTO depensesDTO = (DepensesDTO) o;
        if (depensesDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), depensesDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return "DepensesDTO [description=" + description + ", libelle=" + libelle + ", montant=" + montant + ", date="
				+ date + "]";
	}

    
}
