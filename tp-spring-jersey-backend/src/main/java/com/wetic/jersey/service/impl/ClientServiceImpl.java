package com.wetic.jersey.service.impl;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wetic.jersey.domain.Client;
import com.wetic.jersey.repository.ClientRepository;
import com.wetic.jersey.service.ClientService;
import com.wetic.jersey.service.dto.ClientDTO;
import com.wetic.jersey.service.mapper.ClientMapper;

@Service
public class ClientServiceImpl implements ClientService {

	private final ClientRepository clientRepository;
	private final ClientMapper clientMapper;

	public ClientServiceImpl(ClientRepository clientRepository, ClientMapper clientMapper) {
		this.clientRepository = clientRepository;
		this.clientMapper = clientMapper;
	}

	@Override
	public ClientDTO save(ClientDTO clientDTO) {
		Client client = clientMapper.toEntity(clientDTO);
		client = clientRepository.save(client);
		return clientMapper.toDto(client);
	}

	@Override
	public Page<ClientDTO> findAll(Pageable pageable) {
		return clientRepository.findAll(pageable).map(clientMapper::toDto);
	}

	@Override
	public Optional<ClientDTO> findOne(Long id) {
		return clientRepository.findById(id).map(clientMapper::toDto);
	}

	@Override
	public Page<ClientDTO> findByUser(Pageable pageable, Long id) {
		return clientRepository.findByUser(pageable, id).map(clientMapper::toDto);
	}

	public Long countAll() {
		return clientRepository.CountAll();
	}

	@Override
	public void delete(Long id) {
		clientRepository.deleteById(id);
	}

}
