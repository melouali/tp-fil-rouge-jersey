package com.wetic.jersey.service;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.wetic.jersey.service.dto.DepensesDTO;

public interface DepensesService {

	DepensesDTO save (DepensesDTO depenseDTO);
	
	Page<DepensesDTO> findAll(Pageable pageable);
	
	Optional<DepensesDTO> findOne(Long id);
	
	void delete(Long id);
}
