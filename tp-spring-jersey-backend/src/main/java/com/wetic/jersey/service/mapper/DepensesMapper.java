package com.wetic.jersey.service.mapper;

import org.mapstruct.*;

import com.wetic.jersey.domain.Depense;
import com.wetic.jersey.service.dto.DepensesDTO;

/**
 * Mapper for the entity {@link Depense} and its DTO {@link DepensesDTO}.
 */
@Mapper(componentModel = "spring", uses = { TypeDepenseMapper.class })
public interface DepensesMapper extends EntityMapper<DepensesDTO, Depense> {

	@Mapping(source = "typeDepense.id", target = "typeDepenseId")
	DepensesDTO toDto(Depense depense);

	@Mapping(source = "typeDepenseId", target = "typeDepense")
	Depense toEntity(DepensesDTO depensesDTO);

	default Depense fromId(Long id) {
		if (id == null) {
			return null;
		}
		Depense depense = new Depense();
		depense.setId(id);
		return depense;
	}
}
