package com.wetic.jersey.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.wetic.jersey.service.FactureService;
import com.wetic.jersey.service.dto.FactureDTO;
import com.wetic.jersey.service.dto.FactureDTO;
import com.wetic.jersey.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class FactureResource {

private static final String ENTITY_NAME = "factures";
	
	@Value("${jhipster.clientApp.name}")
	private String applicationName;
	
	private final FactureService factureService;

	public FactureResource(FactureService factureService) {
		this.factureService = factureService;
	}
	
	@GetMapping("/factures")
	public ResponseEntity<List<FactureDTO>> getAllFactures (Pageable pageable, @RequestParam MultiValueMap<String, String> facturesParamQuery,
			UriComponentsBuilder uriBuilder){
		Page<FactureDTO> pageFactures = factureService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(facturesParamQuery), pageFactures);
				return ResponseEntity.ok().headers(headers).body(pageFactures.getContent());
	}
	
	@GetMapping("/factures/{id}")
	public ResponseEntity<FactureDTO> getFacture(@PathVariable Long id){
		Optional<FactureDTO> factureDTO = factureService.findOne(id);
		return ResponseUtil.wrapOrNotFound(factureDTO);
	}
	
	@GetMapping("/factures/client/{id}")
	public ResponseEntity<List<FactureDTO>> getAllFactureByClientId (Pageable pageable, @PathVariable Long id,
			@RequestParam MultiValueMap<String, String> facturesParamQuery, UriComponentsBuilder uriBuilder){
		Page<FactureDTO> pageFacturesByClientId = factureService.findByClient(pageable, id);
		HttpHeaders header = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(facturesParamQuery), pageFacturesByClientId);
		return ResponseEntity.ok().headers(header).body(pageFacturesByClientId.getContent());
	}
	
//	@GetMapping("/factures/{clientId}")
//	public ResponseEntity<FactureDTO> getFactureByClientId(@PathVariable Long id){
//		Optional<FactureDTO> factureDTO = factureService.findByClient(pageable, id)
//	}
	
	@DeleteMapping("/factures/{id}")
	public ResponseEntity<Void> deleteFacture(@PathVariable Long id){
		factureService.delete(id);
		return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
	}
	
	
	@PutMapping("/factures")
	public ResponseEntity<FactureDTO> updateFacture(@Valid @RequestBody FactureDTO factureDTO) throws URISyntaxException {
		if (factureDTO.getId() == null) {
			throw new BadRequestAlertException("pas de facture avec cet id", ENTITY_NAME, "entrez une id correcte");
		}
		FactureDTO retour = factureService.save(factureDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, factureDTO.getId().toString())).body(retour);
	}
	

	
	@PostMapping("/factures")
	public ResponseEntity<FactureDTO> createFacture(@Valid @RequestBody FactureDTO factureDTO)
	throws URISyntaxException{
	if (factureDTO.getId() != null) {
		throw new BadRequestAlertException("Pas d'id à spécifier pour un nouveau produit", ENTITY_NAME, "ce produit existe");
	}
	FactureDTO retour = factureService.save(factureDTO);
	return ResponseEntity.created(new URI("/api/factures" + retour.getId())).headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, retour.getId().toString()))
			.body(retour);
	}
	
}
