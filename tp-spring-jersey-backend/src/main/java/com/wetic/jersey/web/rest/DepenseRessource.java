package com.wetic.jersey.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.wetic.jersey.service.DepensesService;
import com.wetic.jersey.service.dto.DepensesDTO;
import com.wetic.jersey.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class DepenseRessource {
	
	 private static final String ENTITY_NAME = "depenses";
	 @Value("${jhipster.clientApp.name}")
	    private String applicationName;
	 private final DepensesService depensesService;
	
	 public DepenseRessource(DepensesService depensesService) {
		super();
		this.depensesService = depensesService;
	}
	 /**
	     * {@code POST  /depenses} : Create a new depense.
	     *
	     * @param depenseDTO the depenseDTO to create.
	     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new depenseDTO, or with status {@code 400 (Bad Request)} if the depense has already an ID.
	     * @throws URISyntaxException if the Location URI syntax is incorrect.
	     */
	    @PostMapping("/depenses")
	    public ResponseEntity<DepensesDTO> createDepense(@RequestBody DepensesDTO depensesDTO) throws URISyntaxException {
	        if (depensesDTO.getId() != null) {
	            throw new BadRequestAlertException("A new depense cannot already have an ID", ENTITY_NAME, "inexists");
	        }
	        DepensesDTO result = depensesService.save(depensesDTO);
	        return ResponseEntity.created(new URI("/api/depenses/" + result.getId()))
	            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
	            .body(result);
	    }
	 
	    /**
	     * {@code GET  /depenses} : get all the depenses.
	     *
	     * @param pageable the pagination information.
	     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of depenses in body.
	     */
	    @GetMapping("/depenses")
	    public ResponseEntity<List<DepensesDTO>> getAllDepenses(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
	        Page<DepensesDTO> page = depensesService.findAll(pageable);
	        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
	        return ResponseEntity.ok().headers(headers).body(page.getContent());
	    }
	    
	    /**
	     * {@code GET  /depenses/:id} : get the "id" depense.
	     *
	     * @param id the id of the depensesDTO to retrieve.
	     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the depenseDTO, or with status {@code 404 (Not Found)}.
	     */
	    @GetMapping("/depenses/{id}")
	    public ResponseEntity<DepensesDTO> getDepense(@PathVariable Long id) {
	        Optional<DepensesDTO> depensesDTO = depensesService.findOne(id);
	        return ResponseUtil.wrapOrNotFound(depensesDTO);
	    }
	 
	    /**
	     * {@code PUT  /depenses} : Updates an existing depense.
	     *
	     * @param depenseDTO the depenseDTO to update.
	     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated depenseDTO,
	     * or with status {@code 400 (Bad Request)} if the depenseDTO is not valid,
	     * or with status {@code 500 (Internal Server Error)} if the depenseDTO couldn't be updated.
	     * @throws URISyntaxException if the Location URI syntax is incorrect.
	     */
	    @PutMapping("/depenses")
	    public ResponseEntity<DepensesDTO> updateDepense(@RequestBody DepensesDTO depensesDTO) throws URISyntaxException {
	        if (depensesDTO.getId() == null) {
	            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
	        }
	        DepensesDTO result = depensesService.save(depensesDTO);
	        return ResponseEntity.ok()
	            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, depensesDTO.getId().toString()))
	            .body(result);
	    }
	    
	    /**
	     * {@code DELETE  /depenses/:id} : delete the "id" depense.
	     *
	     * @param id the id of the depensesDTO to delete.
	     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
	     */
	    @DeleteMapping("/depenses/{id}")
	    public ResponseEntity<Void> deleteDepense(@PathVariable Long id) {
	        depensesService.delete(id);
	        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
	    }
	 
	 

}
