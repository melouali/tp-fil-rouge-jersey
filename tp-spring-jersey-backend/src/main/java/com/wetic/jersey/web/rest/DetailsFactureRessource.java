package com.wetic.jersey.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.wetic.jersey.service.DetailsFactureService;
import com.wetic.jersey.service.dto.DetailsFactureDTO;
import com.wetic.jersey.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class DetailsFactureRessource {

	 private static final String ENTITY_NAME = "detailsfacture";
	 @Value("${jhipster.clientApp.name}")
	    private String applicationName;
	 private final DetailsFactureService detailsFactureService;
	public DetailsFactureRessource(DetailsFactureService detailsFactureService) {
		super();
		this.detailsFactureService = detailsFactureService;
	}
	 
	/**
     * {@code POST  /detailsfacture} : Create a new detailsfacture.
     *
     * @param detailsFactureDTO the detailsFactureDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new detailsFactureDTO, or with status {@code 400 (Bad Request)} if the detailsFacture has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/details-factures")
    public ResponseEntity<DetailsFactureDTO> createDetailsFacture(@RequestBody DetailsFactureDTO detailsFactureDTO) throws URISyntaxException {
        if (detailsFactureDTO.getId() != null) {
            throw new BadRequestAlertException("A new detail of facture cannot already have an ID", ENTITY_NAME, "inexists");
        }
        DetailsFactureDTO result = detailsFactureService.save(detailsFactureDTO);
        return ResponseEntity.created(new URI("/api/detailsfacture/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
 
    /**
     * {@code GET  /detailsfacture} : get all the detailsfacture.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailsfacture in body.
     */
    @GetMapping("/details-facture")
    public ResponseEntity<List<DetailsFactureDTO>> getAllDetailsFacture(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        Page<DetailsFactureDTO> page = detailsFactureService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    /**
     * {@code GET  /details-facture/:id} : get the "id" details facture.
     *
     * @param id the id of the detailsfactureDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailsfactureDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/details-factures/{id}")
    public ResponseEntity<DetailsFactureDTO> getDetailsFacture(@PathVariable Long id) {
        Optional<DetailsFactureDTO> detailsFactureDTO = detailsFactureService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailsFactureDTO);
    }
 
    /**
     * {@code PUT  /details-facture} : Updates an existing details facture.
     *
     * @param detailsfactureDTO the detailsfactureDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailsfactureDTO,
     * or with status {@code 400 (Bad Request)} if the detailsfactureDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the detailsfactureDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/details-factures")
    public ResponseEntity<DetailsFactureDTO> updateDetailsFacture(@RequestBody DetailsFactureDTO detailsFactureDTO) throws URISyntaxException {
        if (detailsFactureDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DetailsFactureDTO result = detailsFactureService.save(detailsFactureDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, detailsFactureDTO.getId().toString()))
            .body(result);
    }
    
    /**
     * {@code DELETE  /detailsfacture/:id} : delete the "id" detailsfacture.
     *
     * @param id the id of the detailsfactureDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/details-factures/{id}")
    public ResponseEntity<Void> deleteDetailsFacture(@PathVariable Long id) {
        detailsFactureService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    
    @GetMapping("/details-factures/facture/{id}")
    public ResponseEntity<List<DetailsFactureDTO>> getAllDetailsFactureByFactureId(Pageable pageable
    		, @RequestParam MultiValueMap<String, String> queryParams
    		, UriComponentsBuilder uriBuilder, @PathVariable Long id) {
        Page<DetailsFactureDTO> page = detailsFactureService.findByFactureId(pageable, id);
        HttpHeaders headers =
        		PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
