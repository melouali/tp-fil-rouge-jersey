package com.wetic.jersey.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import com.wetic.jersey.service.TypeDepenseService;
import com.wetic.jersey.service.dto.TypeDepenseDTO;
import com.wetic.jersey.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.*;

@RestController
@RequestMapping("/api")
public class TypeDepenseResource {

	private static final String ENTITY_NAME = "typeDepense";

	@Value("${jhipster.clientApp.name}")
	private String applicationName;

	private final TypeDepenseService typeDepenseService;

	public TypeDepenseResource(TypeDepenseService typeDepenseService) {
		this.typeDepenseService = typeDepenseService;
	}

	@PostMapping("/typeDepense")
	public ResponseEntity<TypeDepenseDTO> createTypeDepense(@Valid @RequestBody final TypeDepenseDTO typeDepenseDTO)
			throws URISyntaxException {
		if (typeDepenseDTO.getId() != null) {
			throw new BadRequestAlertException("A new typeDepense cannot already have an ID", ENTITY_NAME, "idexists");
		}
		TypeDepenseDTO result = typeDepenseService.save(typeDepenseDTO);
		return ResponseEntity
				.created(new URI("/api/typeDepense/" + result.getId())).headers(HeaderUtil
						.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	@PutMapping("/typeDepense")
	public ResponseEntity<TypeDepenseDTO> updateTypeDepense(@Valid @RequestBody TypeDepenseDTO typeDepenseDTO)
			throws URISyntaxException {
		if (typeDepenseDTO.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		TypeDepenseDTO result = typeDepenseService.save(typeDepenseDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME,
				typeDepenseDTO.getId().toString())).body(result);
	}

	@GetMapping("/typeDepense/{id}")
	public ResponseEntity<TypeDepenseDTO> getTypeDepense(@PathVariable Long id) {
		Optional<TypeDepenseDTO> typeDepenseDTO = typeDepenseService.findOne(id);
		return ResponseUtil.wrapOrNotFound(typeDepenseDTO);
	}

	@GetMapping("/typeDepense")
	public ResponseEntity<List<TypeDepenseDTO>> getAllTypeDepense(Pageable pageable,
			@RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
		Page<TypeDepenseDTO> page = typeDepenseService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
		return ResponseEntity.ok().headers(headers).body(page.getContent());
	}

	@DeleteMapping("/typeDepense/{id}")
	public ResponseEntity<Void> deleteTypeDepense(@PathVariable Long id) {
		typeDepenseService.delete(id);
		return ResponseEntity.noContent()
				.headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
				.build();
	}

}
