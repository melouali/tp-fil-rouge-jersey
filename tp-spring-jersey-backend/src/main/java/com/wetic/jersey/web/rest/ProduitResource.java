package com.wetic.jersey.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.wetic.jersey.service.ProduitService;
import com.wetic.jersey.service.dto.ProduitDTO;
import com.wetic.jersey.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class ProduitResource {

	private static final String ENTITY_NAME = "produit";
	
	@Value("${jhipster.clientApp.name}")
	private String applicationName;
	
	private final ProduitService produitService;

	// Constructeur
	public ProduitResource(ProduitService produitService) {
		super();
		this.produitService = produitService;
	}
	
	@GetMapping("/produits")
	public ResponseEntity<List<ProduitDTO>> getAllProducts(Pageable pageable,
			@RequestParam MultiValueMap<String, String> productsParamQuery, UriComponentsBuilder uriBuilder){
		Page<ProduitDTO> pageProduits = produitService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(productsParamQuery), pageProduits );
		return ResponseEntity.ok().headers(headers).body(pageProduits.getContent());
	}
	
	@PostMapping("/produits")
	public ResponseEntity<ProduitDTO> createProduct(@Valid @RequestBody ProduitDTO produitDTO)
	throws URISyntaxException{
	if (produitDTO.getId() != null) {
		throw new BadRequestAlertException("Pas d'id à spécifier pour un nouveau produit", ENTITY_NAME, "ce produit existe");
	}
	ProduitDTO retour = produitService.save(produitDTO);
	return ResponseEntity.created(new URI("/api/produits" + retour.getId())).headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, retour.getId().toString()))
			.body(retour);
	}
	
	@GetMapping("/produits/{id}")
	public ResponseEntity<ProduitDTO> getProduct(@PathVariable Long id){
	Optional<ProduitDTO> produitDTO = produitService.findOne(id);
	return ResponseUtil.wrapOrNotFound(produitDTO);
	}
	

	@DeleteMapping("/produits/{id}")
	public ResponseEntity<Void> deleteProduct(@PathVariable Long id){
		produitService.delete(id);
		return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
	}
	
	@PutMapping("/produits")
	public ResponseEntity<ProduitDTO> updateProduct(@Valid @RequestBody ProduitDTO produitDTO) throws URISyntaxException {
		if (produitDTO.getId() == null) {
			throw new BadRequestAlertException("id inexistant", ENTITY_NAME, "entrer un id valide");
		}
		ProduitDTO retour = produitService.save(produitDTO);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, produitDTO.getId().toString())).build();
	}
}


