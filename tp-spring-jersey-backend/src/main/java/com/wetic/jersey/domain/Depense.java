package com.wetic.jersey.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Depense")
public class Depense implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "date_facturation", nullable = false)
	private ZonedDateTime date;

	@Column(name = "description")
	private String description;

	@NotNull
	@Column(name = "libelle", nullable = false)
	private String libelle;

	@NotNull
	@Column(name = "montant", nullable = false)
	private Float montant;

	@ManyToOne
	@JoinColumn(unique = true)
	private TypeDepense typeDepense;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ZonedDateTime getDate() {
		return date;
	}

	public void setDate(ZonedDateTime date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Float getMontant() {
		return montant;
	}

	public void setMontant(Float montant) {
		this.montant = montant;
	}

	public TypeDepense getTypeDepense() {
		return typeDepense;
	}

	public void setTypeDepense(TypeDepense typeDepense) {
		this.typeDepense = typeDepense;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Depense)) {
			return false;
		}
		return id != null && id.equals(((Depense) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "Depense [id=" + id + ", date=" + date + ", description=" + description + ", libelle=" + libelle
				+ ", montant=" + montant + "]";
	}

}
