package com.wetic.jersey.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.BatchSize;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "jhi_user")
public class User extends AbstractAuditingEntity  implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(nullable = false)
	private boolean activated = false;

	@Size(max = 20)
	@Column(name = "Activation_key", length = 20, nullable = true)
	private String activationKey = null;

	@Email
	@Size(max = 254)
	@Column(length = 254, unique = true, nullable = true)
	private String email = null;

	@Size(max = 50)
	@Column(name = "first_name", nullable = true, length = 50)
	private String firstName = null;

	@Size(max = 256)
	@Column(name = "image_url", length = 256, nullable = true)
	private String imageUrl = null;

	@Size(max = 6)
	@Column(name = "lang_key", length = 6, nullable = true)
	private String langKey = null;

	@Size(max = 50)
	@Column(name = "last_name", length = 50, nullable = true)
	private String lastName = null;

	@NotNull
	@Size(max = 50)
	@Column(name = "login", length = 50, nullable = false, unique = true)
	private String login;

	@JsonIgnore
	@NotNull
	@Size(max = 60)
	@Column(name = "Password_Hash", length = 60, nullable = false)
	private String password;

	@Size(max = 20)
	@Column(name = "reset_key", length = 20, nullable = true)
	@JsonIgnore
	private String resetKey = null;

	@Column(name = "reset_date", nullable = true)
	private Instant resetDate = null;

	// Je créé manuellement une table tampon temporaire entre User et Authority
	@JsonIgnore
	@ManyToMany
	@JoinTable(
			name = "jhi_user_authority",
			joinColumns = {@JoinColumn(name = "userId", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "authorityName", referencedColumnName = "name")})
    private Set<Authority> authorities = new HashSet<>();


	public Set<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Set<Authority> authorities) {
		this.authorities = authorities;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}
    public boolean getActivated() {
		return activated;
	}
	public String getActivationKey() {
		return activationKey;
	}

	public void setActivationKey(String activationKey) {
		this.activationKey = activationKey;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getLangKey() {
		return langKey;
	}

	public void setLangKey(String langKey) {
		this.langKey = langKey;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getResetKey() {
		return resetKey;
	}

	public void setResetKey(String resetKey) {
		this.resetKey = resetKey;
	}

	public Instant getResetDate() {
		return resetDate;
	}

	public void setResetDate(Instant resetDate) {
		this.resetDate = resetDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (activated ? 1231 : 1237);
		result = prime * result + ((activationKey == null) ? 0 : activationKey.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imageUrl == null) ? 0 : imageUrl.hashCode());
		result = prime * result + ((langKey == null) ? 0 : langKey.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((resetDate == null) ? 0 : resetDate.hashCode());
		result = prime * result + ((resetKey == null) ? 0 : resetKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (activated != other.activated)
			return false;
		if (activationKey == null) {
			if (other.activationKey != null)
				return false;
		} else if (!activationKey.equals(other.activationKey))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imageUrl == null) {
			if (other.imageUrl != null)
				return false;
		} else if (!imageUrl.equals(other.imageUrl))
			return false;
		if (langKey == null) {
			if (other.langKey != null)
				return false;
		} else if (!langKey.equals(other.langKey))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (resetDate == null) {
			if (other.resetDate != null)
				return false;
		} else if (!resetDate.equals(other.resetDate))
			return false;
		if (resetKey == null) {
			if (other.resetKey != null)
				return false;
		} else if (!resetKey.equals(other.resetKey))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", activated=" + activated + ", activationKey=" + activationKey + ", email=" + email
				+ ", firstName=" + firstName + ", imageUrl=" + imageUrl + ", langKey=" + langKey + ", lastName="
				+ lastName + ", login=" + login + ", password=" + password + ", resetKey=" + resetKey + ", resetDate="
				+ resetDate + "]";
	}

}
