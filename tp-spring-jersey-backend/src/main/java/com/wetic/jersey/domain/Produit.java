package com.wetic.jersey.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.*;


@Entity
@Table(name = "produit")
public class Produit implements Serializable {

	private static final long serialVersionUID = 1L;
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @Column(name = "description", nullable = false)
    private String description;
    
    @NotNull
    @Column(name = "libelle", nullable = false)
    private String libelle;
    
    @NotNull
    @Column(name = "prix", nullable = false)
    private float prix;
    
    @NotNull
    @Column(name = "qteStock", nullable = false)
    private int qteStock; // rename to qteStock
	
	// uncomment some things may be 
    @OneToMany(mappedBy = "produit")
    private Set<DetailsFacture> detailsFacture = new HashSet();

    
    //Accesseurs et mutateurs
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}
	
	public Produit description( String description) {
		this.description = description;
		return this;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLibelle() {
		return libelle;
	}
	
	public Produit libelle(String libelle) {
		this.libelle = libelle;
		return this;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public float getPrix() {
		return prix;
	}

	public Produit prix(float prix) {
		this.prix = prix;
		return this;
	}
	
	public void setPrix(float prix) {
		this.prix = prix;
	}
// rename .....
	public int getQteStock() {
		return qteStock;
	}
	
	public Produit qteStock(int qteStock) {
		this.qteStock = qteStock;
		return this;
	}

	public void setQteStock(int qteStock) {
		this.qteStock = qteStock;
	}

	public Set<DetailsFacture> getDetailsFacture() {
		return detailsFacture;
	}

	public Produit detailsFacture(Set<DetailsFacture> detailsFacture) {
		this.detailsFacture = detailsFacture;
		return this;
	}
	
	public Produit addDetailsFacture(DetailsFacture detailsFacture) {
		this.detailsFacture.add(detailsFacture);
//		detailsFacture.setProduit(this);
		return this;
	}
	
	public Produit removeDetailsFacture(DetailsFacture detailsFacture) {
		this.detailsFacture.remove(detailsFacture);
//		detailsFacture.setProduit(null);
		return this;
	}
	
	public void setDetailsFacture(Set<DetailsFacture> detailsFacture) {
		this.detailsFacture = detailsFacture;
	}

	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {
            return true;
        }
        if (!(obj instanceof Produit)) {
            return false;
        }
        return id != null && id.equals(((Produit) obj).id);
    }

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "Produit [id=" + getId() + ", description=" + getDescription() + ", libelle=" 
				+ getLibelle() + ", detailsFacture=" + getDetailsFacture() + "]";
	}
	
	

    
	
	
	
}
