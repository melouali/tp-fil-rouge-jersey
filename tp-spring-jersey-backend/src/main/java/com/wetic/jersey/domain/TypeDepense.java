package com.wetic.jersey.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "type_depense")
public class TypeDepense implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "date_facturation")
	private Date dateFacturation;

	@Column(name = "description")
	private String description;

	@NotNull
	@Column(name = "libelle", nullable = false)
	private String libelle;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateFacturation() {
		return dateFacturation;
	}

	public void setDateFacturation(Date dateFacturation) {
		this.dateFacturation = dateFacturation;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof TypeDepense)) {
			return false;
		}
		return id != null && id.equals(((TypeDepense) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "TypeDepense{" + "id=" + getId() + 
				", date_facturation='" + getDateFacturation() + "'" +
				", description='" + getDescription() + "'" +
				", libelle='" + getLibelle() + "'" + "}";
	}

}
