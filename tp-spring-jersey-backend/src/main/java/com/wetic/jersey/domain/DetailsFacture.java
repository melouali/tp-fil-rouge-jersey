package com.wetic.jersey.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "details_facture")
public class DetailsFacture implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(name = "qte_produit", nullable = false)
	private int qteProduit;

	@NotNull
	@Column(name = "description", nullable = false)
	private String description;

	@ManyToOne
	@JsonIgnoreProperties("produits")
	private Produit produit;

	@ManyToOne
	@JsonIgnoreProperties("detailsFactures")
	private Facture facture;

	public DetailsFacture() {
	}

	public DetailsFacture(Integer qteProduit, String description, Facture facture, Produit produit) {
		this.qteProduit = qteProduit;
		this.description = description;
		this.facture = facture;
		this.produit = produit;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQteProduit() {
		return qteProduit;
	}

	public DetailsFacture qteProduit(Integer qteProduit) {
		this.qteProduit = qteProduit;
		return this;
	}

	public void setQteProduit(Integer qteProduit) {
		this.qteProduit = qteProduit;
	}

	public DetailsFacture description(String description) {
		this.description = description;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Facture getFacture() {
		return facture;
	}

	public void setFacture(Facture facture) {
		this.facture = facture;
	}

	public Produit getProduit() {
		return produit;
	}

	public DetailsFacture produit(Produit produit) {
		this.produit = produit;
		return this;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

	public DetailsFacture facture(Facture facture) {
		this.facture = facture;
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof DetailsFacture)) {
			return false;
		}
		return id != null && id.equals(((DetailsFacture) o).id);
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public String toString() {
		return "DetailsFacture{" + "id=" + getId() + ", qteProduit=" + getQteProduit() + "}";
	}

}
